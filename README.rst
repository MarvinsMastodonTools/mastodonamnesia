MastodonAmnesia
---------------

!!! MastodonAmnesia has been renamed to **Fedinesia** which is available on PyPI.org !!!

!!! MastodonAmnesia no longer receives fixes or new features !!!

Please update to Fedinesia
==========================

Use the links below for information and to update:

 - https://pypi.org/project/fedinesia/
 - https://codeberg.org/MarvinsMastodonTools/fedinesia
