Changelog
=========

..
   All enhancements and patches to MastodonAmnesia will be documented
   in this file.  It adheres to the structure of http://keepachangelog.com/ ,
   but in reStructuredText instead of Markdown (for ease of incorporation into
   Sphinx documentation and the PyPI description).

   The format is trending towards that described at `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
   and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

Unreleased
----------

See the fragment files in the `changelog.d directory`_.

.. _changelog.d directory: https://codeberg.org/MarvinsMastodonTools/mastodonamnesia/src/branch/main/changelog.d


.. scriv-insert-here

.. _changelog-2.2.2:

2.2.2 — 2023-01-27
==================

Changed
-------

-MastadonAmnesia has been renamed to `Fedinesia`_ and you can find the repo at
 https://codeberg.org/MarvinsMastodonTools/fedinesia
 MastodonAmnesia will no longer be maintained. All new work and bug fixes wil go to `Fedinesia`_.
 With that said `Fedinesia 2.3.0`_ is virtually equivalent to MastodonAmnesia 2.2.1 and 2.2.2

.. _Fedinesia 2.3.0: https://pypi.org/project/fedinesia/2.3.0/
.. _Fedinesia: https://pypi.org/project/fedinesia/

.. _changelog-2.2.1:

2.2.1 — 2023-01-26
==================

Fixed
-----

- Removed short option for `--debug-log-file`. This fixes `issue #13`_

.. _issue #13: https://codeberg.org/MarvinsMastodonTools/mastodonamnesia/issues/13

Changed
-------

- Updated dependencies

.. _changelog-2.2.0:

2.2.0 — 2023-01-25
==================

Added
-----

- Optional commandline option `--limit` or `-l` to limit the number of post being deleted.
  This commandline option takes an integer as argument. If this option is not specified no limit is enforced.

- Optional commandline option `--batch-size` or `-b` to specify how many deletes should be sent to instance as one batch.
  This commandline option takes an integer as argument.
  If this option is not specified, all posts to be deleted will be sent as one big batch.
  A sensible starting value is 10 for most instances.

Changed
-------

- Updated dependencies

- Improved debug logging by including debug log for minimal_activitypub.

.. _changelog-2.1.0:

2.1.0 — 2023-01-02
==================

Added
-----

- Optional audit log file. If specified a log of all toots deleted will be logged to this file.
  Audit log can be enabled by specifing the file name for the audit log by using the
  `--audit-log-file` command line option.

- The style of the audit log file can be set with the `-audit-log-style` command line option.
  The Style defaults to `PLAIN` and currently the following two styles for the audit log file
  have been implemented:

  - `PLAIN` will create a plain text audit log file
  - `CSV` will create an audit log file in CSV format with all values quoted.
    A header record (also quoted) will be added if the audit log file is empty or doesn't yet exist.

Changed
-------

- Now using `click`_ instead of `argparse`

.. _click: https://github.com/pallets/click/

.. _changelog-2.0.3:

2.0.3 — 2022-12-30
==================

Changed
-------

- Removed `rstcheck` in pre-commit checks.
- using `scriv`_ to update this changelog now.
- Updated dependencies

.. _scriv: https://github.com/nedbat/scriv

2.0.2 - 2022-11-11
==================

Changed
-------
- Updated versions of dependencies. In particular newer version of minimal-activitypub that fixes an
  issue when deleting posts.


2.0.1 - 2022-10-14
==================

Changed
-------
- Fixed paging internally through toots / statuses.
- Updated versions of dependencies.


2.0.0 - 2022-09-19
==================

First cut of Pleroma support.

Added
-------
- "--debug-log-file" or "-l" argument to write out a debug log to the file named

Changed
-------
- Now supporting Pleroma servers by using my own ActivityPub library called
  `minimal-activitypub`_
- Removed some un-necessary info from config file. MastodonAmnesia should automatically re-format your
  config file next time it runs.

.. _minimal-activitypub: https://codeberg.org/MarvinsMastodonTools/minimal-activitypub

1.0.0 - 2022-08-30
==================

Added
-------
- "--dry-run" or "-d" argument to print out toots that would be deleted without actually deleting any
- Use of `pip-audit`_ for checking security of libraries

.. _pip-audit: https://pypi.org/project/pip-audit/

Changed
-------
- Using `atoot <https://github.com/popura-network/atoot>`_ instead of mastodon.py to allow use of asyncio.
  This necessitated changing some attributes in the config file. This should be migrated to new attribute
  names during the next run of MastodonAmnesia after upgrading to version 1.0.0
- Using `tqdm`_ instead of alive-progress. Again this allows use of asyncio.

.. _tqdm: https://github.com/tqdm/tqdm

0.6.1 - 2022-08-09
==================

Added
-------
- Publishing new versions to PyPi.org using CI.

Changed
-------
- Updated dependency versions

0.6.0 - 2022-07-01
==================

Added
-------
- Re-added version checking. Now versions checking is done against the latest version published on
  `PyPI`_ using the `outdated`_ library.

.. _PyPI: https://pypi.org
.. _outdated: https://github.com/alexmojaki/outdated

Changed
-------
- Updated dependency versions

0.5.1 - 2022-06-05
==================

Fixed
-------
- Added missing dependency "typing-extensions"

0.5.0 - 2022-06-05
==================

Added
-------
- Ability to skip deleting toots that are polls
- Ability to skip deleting toots that are direct messages / DMs
- Ability to skip deleting toots that have attachments / pictures.
- Ability to skip deleting toots that have been favourited at least x times
- Ability to skip deleting toots that have been boosted / rebloged at least x times

Changed
-------
- Updated dependency versions

0.4.0 - 2022-03-23
==================

Added
-------
- More code quality checks added to pre-commit
- Progress bar using [alive-progress][3]

Changed
-------
- Refactor resulting in removal of unneeded code

Fixed
-------
- Suspected bug in accounting for toots to keep

0.3.2 - 2022-03-19
==================

Fixed
-------
- Included updated poetry files.

Changed
-------
- Upgraded Dev dependencies / requirements versions
- Changed order in which user is asked for configuration values.

0.3.1
==================

Added
-------
- Added steps to ask user if bookmarked / favoured / pinned toots should be deleted when they reach the cut-off age.

0.3.0
==================

Added
-------
- Allow skipping deletion of 'Favourited', 'Bookmarked', and 'Pinned' toots.

Removed
-------
- Version checks, use PyPI / pip for that :)

0.2.3 - 2022-02-14
==================

Changed
-------
- Upgraded Dev dependencies / requirements versions

0.2.2 - 2022-01-31
==================

Changed
-------
- Repackaged for release on Pypi
- Upgraded dependencies / requirements versions to:

  - arrow 1.2.2
  - charset-normalizer 2.0.11
  - httpx 0.22.0
  - rich 11.1.0

0.2.1 - 2022-01-07
==================

Changed
-------
- Updated dependencies:

0.2.0 - 2021-01-31
==================

Added
-------
- Optional command line argument to specify a config file other than the default ``config.json``.

0.1.0 - 2021-01-29
==================
Initial release
